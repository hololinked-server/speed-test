import numpy

none = None 
mixed_type = [5, 'Fooooooooooooooooooooooooooooooo', (2, 3, 2,), dict(A=1, B=2, C=3)]
spectrometer = numpy.random.random(1000).astype(numpy.double)
spectrometer_long = numpy.random.random(10000).astype(numpy.double)
scope = numpy.random.random(100000).astype(numpy.double)
scope_long = numpy.random.random(1000000).astype(numpy.double)
scope_multichannel = numpy.random.random((100000, 4)).astype(numpy.double)
spectrum2D = numpy.random.random((100, 100)).astype(numpy.double) 
raw_image = numpy.random.random((1280, 1024, 3)).astype(numpy.uint8) 
spectrum3D = numpy.random.random((500, 500, 3)).astype(numpy.double) 
spectrum4D = numpy.random.random((100, 100, 3, 3)).astype(numpy.double)
