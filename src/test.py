from hololinked.server import JSONSerializer, PickleSerializer, SerpentSerializer, MsgpackSerializer
from tabulate import tabulate
from dtypes import *


def test_speed(method, number_of_iterations, filename, **kwargs):

    results = dict()

    for serializer in [JSONSerializer(), PickleSerializer(), SerpentSerializer(), MsgpackSerializer()]:

        results[serializer.__class__.__name__] = {
            'none' : method(serializer,
                        none, 
                        number_of_iterations, **kwargs),
            'mixed-type' : method(serializer,
                        mixed_type, 
                        number_of_iterations, **kwargs),
            '1e3 doubles' : method(serializer,
                        spectrometer.tolist() if isinstance(serializer, (JSONSerializer, SerpentSerializer, MsgpackSerializer)) else spectrometer, 
                        number_of_iterations, **kwargs),
            '1e4 doubles' : method(serializer,
                        spectrometer_long.tolist() if isinstance(serializer, (JSONSerializer, SerpentSerializer, MsgpackSerializer)) else spectrometer_long, 
                        number_of_iterations, **kwargs),
            '1e5 double entires' : method(serializer,
                        scope.tolist() if isinstance(serializer, (JSONSerializer, SerpentSerializer, MsgpackSerializer)) else scope, 
                        number_of_iterations, **kwargs),
            '1e6 doubles' : method(serializer,
                        scope_long.tolist() if isinstance(serializer, (JSONSerializer, SerpentSerializer, MsgpackSerializer)) else scope_long, 
                        number_of_iterations, **kwargs),
            '4e5 doubles' : method(serializer,
                        scope_multichannel.tolist() if isinstance(serializer, (JSONSerializer, SerpentSerializer, MsgpackSerializer)) else scope_multichannel, 
                        number_of_iterations, **kwargs),
            '2D 1e4 doubles' : method(serializer,
                        spectrum2D.tolist() if isinstance(serializer, (JSONSerializer, SerpentSerializer, MsgpackSerializer)) else spectrum2D, 
                        number_of_iterations, **kwargs),
            '1M 8 bit pixels (uint8)' : method(serializer,
                        raw_image.tolist() if isinstance(serializer, (JSONSerializer, SerpentSerializer, MsgpackSerializer)) else raw_image, 
                        number_of_iterations, **kwargs),
            '3D 7.5e5 doubles' : method(serializer,
                        spectrum3D.tolist() if isinstance(serializer, (JSONSerializer, SerpentSerializer, MsgpackSerializer)) else spectrum3D, 
                        number_of_iterations, **kwargs),
            '4D 90k doubles' : method(serializer,
                        spectrum4D.tolist() if isinstance(serializer, (JSONSerializer, SerpentSerializer, MsgpackSerializer)) else spectrum4D, 
                        number_of_iterations, **kwargs),
        }   

    results_rows = []

    for serializer_name, results_dict in results.items():
        results_rows.append([serializer_name] + list(results_dict.values()))

    table = tabulate(results_rows, headers=["serializer"] + list(results["JSONSerializer"].keys()), tablefmt="grid")
    print(table)
    with open(filename, "w+") as file:
        file.write(table)