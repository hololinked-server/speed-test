from hololinked.client import ObjectProxy
from hololinked.server import RemoteObject
from hololinked.server.serializers import JSONSerializer, PickleSerializer, MsgpackSerializer, SerpentSerializer
from multiprocessing import Process, Queue
import time 
import psutil 
from dtypes import *
from rpc_remote_pc import RemoteTestServer
from test import test_speed


def server(serializer, **kwargs):
    # serializer = 'msgpack' # PythonBuiltinJSONSerializer()
    R = RemoteObject(instance_name='test-object', rpc_serializer=serializer, **kwargs)
    R.run()

def client(serializer, data, number_of_iterations : int, server_pid : int, result_queue : Queue, **kwargs):
    # serializer = 'msgpack' # PythonBuiltinJSONSerializer()
    try:
        C = ObjectProxy(instance_name='test-object', protocol=kwargs.get("server_protocols"), 
                    socket_address=kwargs.get("socket_address", None), serializer=serializer) # type: RemoteObject

        start_time = time.perf_counter_ns()            
        if serializer != 'pickle' and isinstance(data, numpy.ndarray):
            data = data.tolist()

        for i in range(number_of_iterations):
            C.test_speed(value=data)

        result = ((time.perf_counter_ns() - start_time)/number_of_iterations)/1e6
        result_queue.put(result)
        print(f"{serializer} serializer - time taken is {result} milliseconds per iteration for {len(data) if hasattr(data, '__len__') else data}")
        C.exit()
    except Exception as ex:
        raise ex from None 
    finally:
        if server_pid:
            P = psutil.Process(server_pid)
            P.kill()


def get_serializer_as_string(serializer):
    if isinstance(serializer, JSONSerializer):
        return 'json'
    elif isinstance(serializer, MsgpackSerializer):
        return 'msgpack'
    elif isinstance(serializer, PickleSerializer):
        return 'pickle'
    elif isinstance(serializer, SerpentSerializer):
        return 'serpent'
    else:
        raise NotImplementedError(f"Serializer type not implemented {serializer.__class__.__name__}")


def zmq_same_pc(serializer, data, number_of_iterations, **kwargs):

    serializer = get_serializer_as_string(serializer)

    result_queue = Queue()  
    P1 = Process(target=server, args=(serializer, ), kwargs=kwargs)
    P1.start()
    
    P2 = Process(target=client, args=(serializer, data, number_of_iterations, P1.pid, result_queue), kwargs=kwargs)
    P2.start()

    result =  result_queue.get()
    P2.join()
    return result


def zmq_remote_pc(serializer, data, number_of_iterations):

    serializer = get_serializer_as_string(serializer)

    T = ObjectProxy(instance_name='test-server', protocol='TCP', 
                        socket_address="tcp://127.0.0.1:25001", serializer='json') #type: RemoteTestServer
    T.kill_server()
    T.spawn_server(serializer=serializer, server_protocols='TCP', socket_address="tcp://127.0.0.1:25000")
    
    result_queue = Queue()
    client(serializer, data, number_of_iterations, None, result_queue, 
                server_protocols='TCP', socket_address="tcp://127.0.0.1:25000") # queue is serial
    return result_queue.get()


if __name__ == "__main__":
    # test_speed(zmq_same_pc, 1000, "proxy_tcp.txt", socket_address="tcp://127.0.0.1:25000", server_protocols='TCP')
    # test_speed(zmq_same_pc, 1000, "proxy_ipc.txt", server_protocols="IPC")
    test_speed(zmq_remote_pc, 10, 'zmq_remote_pc.txt')

    T = ObjectProxy(instance_name='test-server', protocol='TCP', 
                socket_address="tcp://127.0.0.1:25001", serializer='json') #type: RemoteTestServer
    T.kill_server()
    T.exit()