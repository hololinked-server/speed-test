from multiprocessing import Process
import psutil
from hololinked.server import RemoteObject, remote_method#
from hololinked.server.remote_parameters import Boolean


def server(serializer, **kwargs):
    # serializer = 'msgpack' # PythonBuiltinJSONSerializer()
    R = RemoteObject(instance_name='test-object', rpc_serializer=serializer, **kwargs)
    R.run()


class RemoteTestServer(RemoteObject):
    """
    In principle the same PC script can also use this object
    """
    
    def __init__(self, instance_name : str):
        super().__init__(instance_name=instance_name, server_protocols='TCP', 
                        socket_address="tcp://127.0.0.1:25001", rpc_serializer='json')
        self.child = None 

    @remote_method()
    def kill_server(self):
        if self.child is not None:
            P = psutil.Process(self.child.pid)
            P.kill()
            self.child = None

    @remote_method()
    def spawn_server(self, serializer, **kwargs):
        self.child = Process(target=server, args=(serializer, ), kwargs=kwargs)
        self.logger.info(f"starting remote object test-object with serialiezr {serializer} and kwargs {kwargs}")
        self.child.start()
    

if __name__ == "__main__":
    T = RemoteTestServer(instance_name='test-server')
    T.run()