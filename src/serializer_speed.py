import time 
import numpy 
from hololinked.server import BaseSerializer 
from test import test_speed

def serializer_speed(serializer : BaseSerializer, data : numpy.ndarray, number_of_iterations : int):

    start_time = time.perf_counter_ns()
    for i in range(number_of_iterations):
        serializer.dumps(data)
    end_time = ((time.perf_counter_ns() - start_time)/number_of_iterations)/1e6
    if hasattr(data, '__len__'):
        print("done ", serializer.__class__.__name__, data.shape if isinstance(data, numpy.ndarray) else len(data))
    return end_time

test_speed(serializer_speed, 1000, 'serializers.txt')